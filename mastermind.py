"""
Mastermind.py
Author: Richard Harrington
Last update: 9/3/2013

A simple color-matching game
The master draws four pegs, and the player attempts to
match the color and position of the pegs.

"""
import random
import math

colors=['red','orange','green','blue','yellow','black','white','purple']
master=[]
player=[]
match=[]
difficulty=""
printcolors=""

def deal():
# assign four pegs to the master
    # Make sure the master list is clean
    while len(master)>0:
        master.pop(len(master)-1)

    while len(master)<4:
        a=int(math.floor((random.random()*8)))
        if colors[a] not in master:
            # This makes sure we get four unique colored pegs
            master.append(colors[a])

def play():
# Prompt player for four colors to input
    print(" ".join(colors))
    # Make sure we have a clean list to play with
    while len(player)>0:
        player.pop(len(player)-1)

    for color in str.split(input("Please input four colors :\n")):
        # Make sure the user's inputs are in the valid list
        while color.lower() not in colors:
            color=input("Please input a valid color from the list:\n")

        player.append(color.lower())
            
def score():
# Compare the player's guesses to the master
    score=0
    # Make sure the Match list is clean
    while len(match)>0:
        match.pop(len(match)-1)

    for i in range(0,len(player)):
        # First check for exact matches
        if(master[i]==player[i]):
           score+=25
           match.append("Match")
        elif(player[i] in master):
            # If the peg doesn't match, check to see if the color is a hit  
            score+=10
            match.append("Close")
        else:
            match.append("No Match")
    return score

def set_difficulty():
# Set difficulty level
    d=False
    while d is False:
        difficulty=input("Select difficulty: enter E for easy or H for hard:\n")
        if difficulty.lower() == "e":
            d="easy"
        elif difficulty.lower() == "h":
            d="hard"
        else:
            print("Please enter E for easy or H for hard:\n")
    return d

def play_game():
# Play the game. The player gets n guesses per game.
    n=20
    pscore=0
    d=set_difficulty()
    deal()

    for i in range(1,n+1):
        if pscore==100: break
        else:
            player=[]
            play()
            pscore=score()
            print("Score: "+str(pscore))
        if d=="easy":
            print(" ".join(match))
        else:
            print("Turn: "+i)
            
def main():
# Main loop
    new_game=True
    while new_game:
        play_game()
        print(" ".join(master))
        if input("Play again? y/n: \n")=="n":
            new_game=False

main()

"""
def test_run():
# Used for testing for syntax/logic errors
    deal()
    print(master)
    play()
    print(player)
    print(score())
    print(match)
    
#test_run()
"""
